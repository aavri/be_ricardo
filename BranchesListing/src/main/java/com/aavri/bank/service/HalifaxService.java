package com.aavri.bank.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.aavri.bank.dao.HalifaxDAO;
import com.aavri.bank.exception.BranchesException;
import com.aavri.bank.model.branch.BranchDetails;
import com.aavri.bank.model.halifax.HalifaxBranch;

@Service
public class HalifaxService {

	@Autowired
	HalifaxDAO halifaxDAO;
	
	public List<BranchDetails> getAllBranchesList() throws BranchesException {

		List<HalifaxBranch> listBranches = halifaxDAO.listAllBranches();

		List<BranchDetails> returnList = new ArrayList<BranchDetails>();

		listBranches.stream()
				.forEach(b -> returnList.add(new BranchDetails(
						b.getName(),
						Float.parseFloat(b.getPostalAddress().getGeoLocation().getGeographicCoordinates().getLatitude()),
						Float.parseFloat(b.getPostalAddress().getGeoLocation().getGeographicCoordinates().getLongitude()),
						b.getPostalAddress().getAddressLine().stream().findFirst().orElse(""),
						b.getPostalAddress().getTownName(),
						b.getPostalAddress().getCountrySubDivision().stream().findFirst().orElse(""),
						b.getPostalAddress().getCountry(), 
						b.getPostalAddress().getPostCode())));

		return returnList;
	}

	public List<BranchDetails> getBranchesByCityList(String cityName) throws BranchesException {

		List<HalifaxBranch> listBranches = halifaxDAO.listAllBranches();

		List<BranchDetails> returnList = new ArrayList<BranchDetails>();

		listBranches.stream()
				.filter(b -> cityName.equalsIgnoreCase(b.getPostalAddress().getTownName()))
				.forEach(b -> returnList.add(new BranchDetails(
						b.getName(),
						Float.parseFloat(b.getPostalAddress().getGeoLocation().getGeographicCoordinates().getLatitude()),
						Float.parseFloat(b.getPostalAddress().getGeoLocation().getGeographicCoordinates().getLongitude()),
						b.getPostalAddress().getAddressLine().stream().findFirst().orElse(""),
						b.getPostalAddress().getTownName(),
						b.getPostalAddress().getCountrySubDivision().stream().findFirst().orElse(""),
						b.getPostalAddress().getCountry(), 
						b.getPostalAddress().getPostCode())));

		if (returnList.isEmpty()) {
			throw new BranchesException(HttpStatus.BAD_REQUEST, "No branch was found in the City informed");
		}
		
		return returnList;
	}
	
	public List<BranchDetails> getBranchesByCityAndId(String cityName, String id) throws BranchesException {
		
		List<HalifaxBranch> listBranches = halifaxDAO.listAllBranches();
		
		List<BranchDetails> returnList = new ArrayList<BranchDetails>();
		
		listBranches.stream()
		.filter(b -> cityName.equalsIgnoreCase(b.getPostalAddress().getTownName()) && id.equals(b.getIdentification()))
		.forEach(b -> returnList.add(new BranchDetails(
				b.getName(),
				Float.parseFloat(b.getPostalAddress().getGeoLocation().getGeographicCoordinates().getLatitude()),
				Float.parseFloat(b.getPostalAddress().getGeoLocation().getGeographicCoordinates().getLongitude()),
				b.getPostalAddress().getAddressLine().stream().findFirst().orElse(""),
				b.getPostalAddress().getTownName(),
				b.getPostalAddress().getCountrySubDivision().stream().findFirst().orElse(""),
				b.getPostalAddress().getCountry(), 
				b.getPostalAddress().getPostCode())));

		if (returnList.isEmpty()) {
			throw new BranchesException(HttpStatus.BAD_REQUEST, "No branch was found with the City and ID informed");
		}

		return returnList;
	}

}
