package com.aavri.bank.model.halifax;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class HalifaxJson {
	
	@JsonProperty("data")
	private List<HalifaxData> data = null;

	public List<HalifaxData> getData() {
		return data;
	}

	public void setData(List<HalifaxData> data) {
		this.data = data;
	}
}