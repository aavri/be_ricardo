package com.aavri.bank.model.halifax;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class HalifaxData {

	@JsonProperty("Brand")
	private List<HalifaxBrand> brand = null;

	public List<HalifaxBrand> getBrand() {
		return brand;
	}

	public void setBrand(List<HalifaxBrand> brand) {
		this.brand = brand;
	}
}