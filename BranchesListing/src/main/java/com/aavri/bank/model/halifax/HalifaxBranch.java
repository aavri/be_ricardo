package com.aavri.bank.model.halifax;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class HalifaxBranch {

	@JsonProperty("Identification")
	private String identification;
	
	@JsonProperty("Name")
	private String name;
	
	@JsonProperty("PostalAddress")
	private HalifaxPostalAddress postalAddress;

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HalifaxPostalAddress getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(HalifaxPostalAddress postalAddress) {
		this.postalAddress = postalAddress;
	}

}