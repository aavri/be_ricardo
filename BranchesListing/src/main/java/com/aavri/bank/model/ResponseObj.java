package com.aavri.bank.model;

public class ResponseObj {

	private String message;
	
	public ResponseObj(String message) {
		setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
