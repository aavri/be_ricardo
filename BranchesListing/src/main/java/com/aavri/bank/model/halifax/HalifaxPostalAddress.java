package com.aavri.bank.model.halifax;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class HalifaxPostalAddress {

	@JsonProperty("AddressLine")
	private List<String> addressLine = null;
	
	@JsonProperty("TownName")
	private String townName;
	
	@JsonProperty("CountrySubDivision")
	private List<String> countrySubDivision = null;
	
	@JsonProperty("Country")
	private String country;
	
	@JsonProperty("PostCode")
	private String postCode;
	
	@JsonProperty("GeoLocation")
	private HalifaxGeoLocation geoLocation;

	public List<String> getAddressLine() {
		if (addressLine == null) {
			addressLine = new ArrayList<>(1);
			addressLine.add("");
		}
		return addressLine;
	}

	public void setAddressLine(List<String> addressLine) {
		this.addressLine = addressLine;
	}

	public String getTownName() {
		return townName;
	}

	public void setTownName(String townName) {
		this.townName = townName;
	}

	public List<String> getCountrySubDivision() {
		if (countrySubDivision == null) {
			countrySubDivision = new ArrayList<>(1);
			countrySubDivision.add("");
		}
		return countrySubDivision;
	}

	public void setCountrySubDivision(List<String> countrySubDivision) {
		this.countrySubDivision = countrySubDivision;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public HalifaxGeoLocation getGeoLocation() {
		return geoLocation;
	}

	public void setGeoLocation(HalifaxGeoLocation geoLocation) {
		this.geoLocation = geoLocation;
	}
}