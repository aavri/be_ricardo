package com.aavri.bank.model.halifax;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class HalifaxGeoLocation {

	@JsonProperty("GeographicCoordinates")
	private HalifaxGeographicCoordinates geographicCoordinates;

	public HalifaxGeographicCoordinates getGeographicCoordinates() {
		return geographicCoordinates;
	}

	public void setGeographicCoordinates(HalifaxGeographicCoordinates geographicCoordinates) {
		this.geographicCoordinates = geographicCoordinates;
	}
}