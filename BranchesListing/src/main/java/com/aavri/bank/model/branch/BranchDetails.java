package com.aavri.bank.model.branch;

public final class BranchDetails {

	private String branchName;
	private float latitude;
	private float longitude;
	private String streetAddress;
	private String city;
	private String countrySubDivision;
	private String country;
	private String postCode;

	public BranchDetails(String branchName, float latitude, float longitude, String streetAddress, String city,
			String countrySubDivision, String country, String postCode) {
		super();
		this.branchName = branchName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.streetAddress = streetAddress;
		this.city = city;
		this.countrySubDivision = countrySubDivision;
		this.country = country;
		this.postCode = postCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountrySubDivision() {
		return countrySubDivision;
	}

	public void setCountrySubDivision(String countrySubDivision) {
		this.countrySubDivision = countrySubDivision;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

}