package com.aavri.bank.model.halifax;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class HalifaxBrand {

	@JsonProperty("Branch")
	private List<HalifaxBranch> branch = null;

	public List<HalifaxBranch> getBranch() {
		return branch;
	}

	public void setBranch(List<HalifaxBranch> branch) {
		this.branch = branch;
	}
}