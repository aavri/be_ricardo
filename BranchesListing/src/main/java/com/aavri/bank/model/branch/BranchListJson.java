package com.aavri.bank.model.branch;

import java.util.List;

public class BranchListJson {
	
	private List<BranchDetails> branches;
	
	public BranchListJson(List<BranchDetails> branches) {
		this.branches = branches;
	}
	
	public List<BranchDetails> getBranches() {
		return branches;
	}

	public void setBranches(List<BranchDetails> branches) {
		this.branches = branches;
	}
}