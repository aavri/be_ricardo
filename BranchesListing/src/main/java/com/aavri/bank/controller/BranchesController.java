package com.aavri.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aavri.bank.exception.BranchesException;
import com.aavri.bank.model.ResponseObj;
import com.aavri.bank.model.branch.BranchDetails;
import com.aavri.bank.model.branch.BranchListJson;
import com.aavri.bank.service.HalifaxService;

@RestController
public class BranchesController {

	@Autowired
	private HalifaxService halifaxService;
	
	@RequestMapping(method=RequestMethod.GET, path="/branches")
	public ResponseEntity<?> listAllBranches() {
		List<BranchDetails> listBranches;
		
		try {
			listBranches = halifaxService.getAllBranchesList();
			
		} catch (BranchesException e) {
			return new ResponseEntity<ResponseObj>(new ResponseObj(e.getMessage()), e.getHttpStatus());
		}
		
		return new ResponseEntity<>(new BranchListJson(listBranches), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/branches/{cityName}")
	public ResponseEntity<?> listBranchesByCity(@PathVariable(value="cityName") String paramCityName) {
		
		List<BranchDetails> listBranches;
		
		try {
			listBranches = halifaxService.getBranchesByCityList(paramCityName);
			
		} catch (BranchesException e) {
			return new ResponseEntity<ResponseObj>(new ResponseObj(e.getMessage()), e.getHttpStatus());
		}
		
		return new ResponseEntity<>(new BranchListJson(listBranches), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/branches/{cityName}/{id}")
	public ResponseEntity<?> listBranchesByCityAndId(@PathVariable(value="cityName") String paramCityName, @PathVariable(value="id") String paramId) {
		
		List<BranchDetails> listBranches;
		
		try {
			listBranches = halifaxService.getBranchesByCityAndId(paramCityName, paramId);
			
		} catch (BranchesException e) {
			return new ResponseEntity<ResponseObj>(new ResponseObj(e.getMessage()), e.getHttpStatus());
		}
		
		return new ResponseEntity<>(new BranchListJson(listBranches), HttpStatus.OK);
	}

}