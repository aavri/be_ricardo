package com.aavri.bank.dao;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.aavri.bank.exception.BranchesException;
import com.aavri.bank.model.halifax.HalifaxBranch;
import com.aavri.bank.model.halifax.HalifaxBrand;
import com.aavri.bank.model.halifax.HalifaxData;
import com.aavri.bank.model.halifax.HalifaxJson;

@Repository
public class HalifaxDAO {

	private static final String URL_JSON = "https://api.halifax.co.uk/open-banking/v2.2/branches";
	
	private HalifaxJson halifaxBranches;
	
	/*
	 * Gets the list of all branches from Halifax's API JSON obtained
	 */
	public List<HalifaxBranch> listAllBranches() throws BranchesException {
		try {
			HalifaxData data = getHalifaxBranches().getData().get(0);
			HalifaxBrand brand = data.getBrand().get(0);
			return brand.getBranch();
		} catch (NullPointerException | IndexOutOfBoundsException e) {
			throw new BranchesException(HttpStatus.INTERNAL_SERVER_ERROR, "Halifax data returned is corrupted");
		}
	}
	
	/*
	 * Makes a GET request to the Halifax Bank Branches API 
	 */
	private HalifaxJson getHalifaxBranches() throws BranchesException {
		if (this.halifaxBranches==null) {
			try {
				this.halifaxBranches = new RestTemplate().getForObject(
						URL_JSON,
						HalifaxJson.class);
			} catch (HttpClientErrorException e) {
				throw new BranchesException(HttpStatus.INTERNAL_SERVER_ERROR, "Error accessing Halifax data. Code: " + e.getStatusCode());
			}
		}
		
		return this.halifaxBranches;
	}
	
}
