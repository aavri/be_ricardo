package com.aavri.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BranchesListingApplication {

	public static void main(String[] args) {
		SpringApplication.run(BranchesListingApplication.class, args);
	}
}
