package com.aavri.bank.exception;

import org.springframework.http.HttpStatus;

public class BranchesException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

	    public HttpStatus getHttpStatus() {
	        return httpStatus;
	    }

	    /**
	     * Constructs a new runtime exception with the specified detail message.
	     */
	    public BranchesException(HttpStatus httpStatus, String message) {
	        super(message);
	        this.httpStatus = httpStatus;
	    }
	
}