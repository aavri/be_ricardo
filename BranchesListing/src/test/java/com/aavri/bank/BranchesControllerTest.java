package com.aavri.bank;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.json.JSONException;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.aavri.bank.model.branch.BranchDetails;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class BranchesControllerTest {

	@Test
	public void testBadUrl() throws JSONException {
		RestAssured.get("/AAAAAAA").then().statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	@Test
	public void testListAll() throws JSONException {
		JsonPath jsonPath = RestAssured.get("/branches").andReturn().jsonPath();
		List<BranchDetails> branchList = jsonPath.getList("branches", BranchDetails.class);
		assertFalse(branchList.isEmpty());
	}
	
	@Test
	public void testListCityFound() throws JSONException {
		JsonPath jsonPath = RestAssured.get("/branches/BIRMINGHAM").andReturn().jsonPath();
		List<BranchDetails> branchList = jsonPath.getList("branches", BranchDetails.class);
		assertFalse(branchList.isEmpty());
		
		//Check if the quantity of returned branches matches the expected for the filter
		assertEquals(12, branchList.size());
	}
	
	@Test
	public void testListCityNotFound() throws JSONException {
		RestAssured.get("/branches/AAAAAAAAA").then().statusCode(HttpStatus.BAD_REQUEST.value());
	}
	
	@Test
	public void testListCityAndIdFound() throws JSONException {
		JsonPath jsonPath = RestAssured.get("/branches/BIRMINGHAM/11006800").andReturn().jsonPath();
		List<BranchDetails> branchList = jsonPath.getList("branches", BranchDetails.class);
		assertFalse(branchList.isEmpty());
		
		//Check if the branch returned is the one desired
		String branchName = branchList.get(0).getBranchName();
		assertEquals("BIRMINGHAM THE BULL RING", branchName);
	}
	
	@Test
	public void testListCityAndIdNotFound() throws JSONException {
		RestAssured.get("/branches/BIRMINGHAM/99999999").then().statusCode(HttpStatus.BAD_REQUEST.value());
	}

}
