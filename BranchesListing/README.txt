﻿### Branches Listing API ###
RESTful
SpringBoot + Tomcat Embedded

#-- Objective --#
Provide a RESTful web service which a frontend developer could use to display
branch locations in a customer-facing web application.

The application obtains the list of branches from Halifax’s open banking API,
perform the necessary transformations on the data, and make it accessible to
clients calling this API.

#-- Requirements --#
The API exposes two endpoints:
 - The first provides a list of all branch locations.
 - The second returns a list of branch locations filtered by city name,
   matching a search term sent by the client.
 - An extra function provides a matching branch with the city name and
   id sent by the client.

The response body provided to clients is a JSON object containing an
array of branches.

A branch in the response conforms to the following format:
    branchName          String
    latitude            float
    longitude           float
    streetAddress       String
    city                String
    countrySubDivision  String
    country             String
    postCode            String

#-- Configuration --#
This is a Spring boot application with a embedded Tomcat Server. It runs the BranchesListingApplication.java Main file as default.

#-- How to build --#
Import the existing project into your workspace and run a full Maven update and build

#-- How to run --#
Simply execute the jar file according to your O.S. (i.e. Windows -> "java -jar branches-listing-0.0.1-SNAPSHOT.jar")

#-- How to use: Valid Urls --#
[GET] http://localhost:8080/branches (returns a list of all branches)
[GET] http://localhost:8080/branches/{cityName} (returns a list of all branches in an informed city)
[GET] http://localhost:8080/branches/{cityName}/{id} (returns the branch with the city and id specified)

#-- Expected Responses Status Code --#
200 - OK    - The data was successfully obtained from the source and returned to user
400 - ERROR - The search term has no matching results
404 - ERROR - The URL informed is invalid
500 - ERROR - Some problem occurred in the server. It is accompanied by an informative message
